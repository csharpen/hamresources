# Ham Resources


## Getting started

Nothing much to see here yet. Tinkering around with a shared python library of some of the things I find myself wanting in multiple tools (mostly unpublished). Perhaps someone else needs them too. I may package these up properly and follow correct conventions someday as I get around to abstracting stuff into it.

## Usage examples
### Search the ARRL Considerate Operator's Guide for a frequency
```
from HamResources import HamResources
hr = HamResources()
result = hr.searchFrequency(28.2)
if result['name'] is not None:
    print(f"Name: {result['name']}, Range:  {result['rangeStart']} - {result['rangeEnd']}MHz")
```
Result: 
```Name: Beacons, Range:  28.19 - 28.225MHz```
