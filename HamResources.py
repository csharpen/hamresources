import tkinter as tk
from tkinter import ttk

class HamResources:
    """
    A class representing a collection of ham radio resources.

    Attributes:
        considerateFrequencies (dict): A dictionary mapping of the ARRL Considerate Operator's Frequency Guide frequency ranges
                                       (as tuple of start and end frequency) to their corresponding usage modes or descriptions.
                                       See this PDF for more information: http://www.arrl.org/files/file/conop.pdf
                                       It does not (at present, or maybe ever?) take into account the bandwidth that may extend beyond
                                       the single frequency or range (i.e.: SSB may be + or - 3kHz of a phone calling frequency).
    """
    def __init__(self):
        self.considerateFrequencies = {
            (1.8, 2.0): 'CW',
            (1.8, 1.81): 'Digital Modes',  
            (1.81, 1.81): 'CW QRP calling frequency',
            (1.843, 2.0): 'SSB, SSTV and other wideband modes',
            (1.91, 1.91): 'SSB QRP',
            (1.995, 2.0): 'Experimental',
            (1.999, 2.0): 'Beacons',
            (3.5, 3.51): 'CW DX window',
            (3.56, 3.56): 'QRP CW calling frequency',
            (3.57, 3.6): 'RTTY/Data',
            (3.585, 3.6): 'Automatically controlled data stations',
            (3.59, 3.59): 'RTTY/Data DX',
            (3.79, 3.8): 'DX window',
            (3.845, 3.845): 'SSTV',
            (3.885, 3.885): 'AM calling frequency',
            (3.985, 3.985): 'QRP SSB calling frequency',
            (7.03, 7.03): 'QRP CW calling frequency',
            (7.04, 7.04): 'RTTY/Data DX',
            (7.07, 7.125): 'RTTY/Data',
            (7.1, 7.105): 'Automatically controlled data stations',
            (7.171, 7.171): 'SSTV',
            (7.173, 7.173): 'D-SSTV',
            (7.285, 7.285): 'QRP SSB calling frequency',
            (7.29, 7.29): 'AM calling frequency',
            (10.13, 10.14): 'RTTY/Data',
            (10.14, 10.15): 'Automatically controlled data stations',
            (14.06, 14.06): 'QRP CW calling frequency',
            (14.07, 14.095): 'RTTY/Data',
            (14.095, 14.0995): 'Automatically controlled data stations',
            (14.1, 14.1): 'IBP/NCDXF beacons',
            (14.1005, 14.112): 'Automatically controlled data stations',
            (14.23, 14.23): 'SSTV',
            (14.233, 14.233): 'D-SSTV',
            (14.236, 14.236): 'Digital Voice',
            (14.285, 14.285): 'QRP SSB calling frequency',
            (14.286, 14.286): 'AM calling frequency',
            (18.1, 18.105): 'RTTY/Data',
            (18.105, 18.11): 'Automatically controlled data stations',
            (18.11, 18.11): 'IBP/NCDXF beacons',
            (18.1625, 18.1625): 'Digital Voice',
            (21.06, 21.06): 'QRP CW calling frequency',
            (21.07, 21.11): 'RTTY/Data',
            (21.09, 21.1): 'Automatically controlled data stations',
            (21.15, 21.15): 'IBP/NCDXF beacons',
            (21.34, 21.34): 'SSTV',
            (21.385, 21.385): 'QRP SSB calling frequency', 
            (24.92, 24.925): 'RTTY/Data',
            (24.925, 24.93): 'Automatically controlled data stations',
            (24.93, 24.93): 'IBP/NCDXF beacons',
            (28.06, 28.06): 'QRP CW calling frequency',
            (28.07, 28.12): 'RTTY/Data',
            (28.12, 28.189): 'Automatically controlled data stations',
            (28.19, 28.225): 'Beacons',
            (28.2, 28.2): 'IBP/NCDXF beacons',
            (28.385, 28.385): 'QRP SSB calling frequency',
            (28.68, 28.68): 'SSTV',
            (29.0, 29.2): 'AM',
            (29.3, 29.51): 'Satellite downlinks',
            (29.52, 29.58): 'Repeater inputs',
            (29.6, 29.6): 'FM simplex',
            (29.62, 29.68): 'Repeater outputs' 
        }

    def searchFrequency(self, frequency):
        """
        Searches the ARRL Considerate Operator's Guide for a given frequency within the predefined frequency ranges.

        :param frequency: The frequency value (in MHz) to be searched for.
        :type frequency: float
        
        :return: A dictionary containing the description, start, and end of the matched frequency range.
                 If no match is found, it returns a dictionary with None values. 
                 Checking `result['name'] is not None` is likely a sensible simple Boolean test.
        :rtype: dict
        """
        for range, name in self.considerateFrequencies.items():
            if range[0] <= frequency <= range[1]:
                return {
                    "name": name,
                    "rangeStart": range[0],
                    "rangeEnd": range[1]
                }
        return {
            "name": None,
            "rangeStart": None,
            "rangeEnd": None
        }

class Tooltip:
    def __init__(self, widget, text):
        self.widget = widget
        self.text = text
        self.tooltip = None
        self.widget.bind("<Enter>", self.show)
        self.widget.bind("<Leave>", self.hide)
        self.widget.bind("<FocusOut>", self.hide)

    def show(self, event=None):
        x, y, _, _ = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 50
        y += self.widget.winfo_rooty() + 50

        self.tooltip = tk.Toplevel(self.widget)
        self.tooltip.wm_overrideredirect(True)
        self.tooltip.wm_geometry(f"+{x}+{y}")

        label = ttk.Label(self.tooltip, text=self.text, background="#ffffe0", relief="solid", borderwidth=1)
        label.pack()

    def hide(self, event=None):
        if self.tooltip:
            self.tooltip.destroy()
            self.tooltip = None

    def update_text(self, new_text):
        self.text = new_text
        if self.tooltip:
            # Assuming the label widget inside the tooltip is the only child
            self.tooltip.winfo_children()[0].config(text=new_text)
